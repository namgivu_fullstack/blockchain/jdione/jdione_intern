# member

| timestamp/id            | birth | email                                                | entry card | location (district in Saigon) | git                                                |
| ----------------------- | ----- | ---------------------------------------------------- | ---------- | ----------------------------- | -------------------------------------------------- |
| Nam G VU                | 1982  | namgivu@gmail.com                                    | done       | 10                            | gitlab namgivu, github namgivu                     |
| Hoang Minh Hien         | 2002  | hien.hoangwerner@hcmut.edu.vn                        | done       | 4                             | gitlab hien.hoangwerner, github hien17             |
| Ngô Công Hiệp           | 1997  | hiep.ngo1609@hcmut.edu.vn, hiepngo136@gmail.com      | .          | 11                            | git lab hiepngo1609, github HiepNgo1609            |
| Lưu Trịnh Thiên         | 2002  | thien.luutrinh@hcmut.edu.vn, luutrinhthien@gmail.com | .          | tan phu                       | gitlab thienluutrinh, github luutrinhthien         |
| Lê Văn Thắng            | 2001  | vanthang23032001@gmail.com                           | .          | 9                             | gitlab levanthang2332001, github levanthang2332001 |
| Nguyen Dang Binh Nguyen | 2002  | nguyen.nguyendt456@hcmut.edu.vn                      | .          | Tan Binh                      | gitlab nguyendt456, github nguyendt456             |
| Hoàng Công Kha          | 2002  | kha.hc12134@sinhvien.hoasen.edu.vn                   | .          | 3                             | gitlab ngochaong123, gitlab ngochaong123           |
