function App() {
  return (
    <>
      <h3>JDI One Intern</h3>
      <table class="table">
        <thead>
          <tr>
            <th>timestamp/id</th>
            <th>birth</th>
            <th>email</th>
            <th>entry card</th>
            <th>location (district in Saigon)</th>
            <th>git</th>
          </tr>
        </thead>

        <tbody>
          <tr>
            <td>1</td>
            <td>1982</td>
            <td>namgivu@gmail.com</td>
            <td>True</td>
            <td>10</td>
            <td>gitlab namgivu, github namgivu</td>
          </tr>

          <tr>
            <td>2</td>
            <td>2001</td>
            <td>vanthang23032001@gmail.com</td>
            <td>.</td>
            <td>9</td>
            <td>gitlab levanthang2332001, github levanthang2332001</td>
          </tr>

          <tr>
            <td>.</td>
            <td>2002</td>
            <td>nguyen.nguyendt456@hcmut.edu.vn</td>
            <td>.</td>
            <td>Tan Binh</td>
            <td>gitlab nguyendt456, github nguyendt456</td>
          </tr>

          <tr>
            <td>2</td>
            <td>2002</td>
            <td>timowerner11germany@gmail.com</td>
            <td>True</td>
            <td>4</td>
            <td>gitlab hien.hoangwerner, github hien17</td>
          </tr>

          <tr>
            <td>1</td>
            <td>2002</td>
            <td>hoangcongkha96@gmail.com</td>
            <td>True</td>
            <td>5</td>
            <td>gitlab ngochaong123, github ngochaong123</td>
          </tr>
        
        </tbody>
      </table>
    </>
  );
}

export default App;
